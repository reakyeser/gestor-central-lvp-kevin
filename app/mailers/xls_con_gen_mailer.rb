class XlsConGenMailer < ApplicationMailer

  # default from: "kmprevencion@liverpool.com.mx"
  default from: "krodriguez@kssoluciones.com.mx"

  def send_con_gen_xls_es(fecha_comienzo, user_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @use = "#{user_log.name} #{user_log.last_name}"
    @name_file = name_file
    @path_file = path_file
    @fin = fecha_termino

    attachments[@name_file] = File.read("#{@path_file}")
    mail to: user_log.email, subject: "Exportación de Resultados de Consulta General a Excel lista: #{@name_file}"
  end

  def send_con_gen_xls_en(fecha_comienzo, user_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @use = "#{user_log.name} #{user_log.last_name}"
    @name_file = name_file
    @path_file = path_file
    @fin = fecha_termino

    attachments[@name_file] = File.read("#{@path_file}")
    mail to: user_log.email, subject: "Result Export from General Search to Excel is ready: #{@name_file}"
  end
end
