class XlsMailer < ApplicationMailer

  # default from: "kmprevencion@liverpool.com.mx"
  default from: "krodriguez@kssoluciones.com.mx"

  ### AlertTran y alertPer
  def send_xls_es(fecha_comienzo, usuario, file, fecha_termino)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/alerts/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")
    mail to: usuario.email , subject: "Exportación de Alerta a Excel lista: #{@file.to_s}"
  end
  ### AlertTran y alertPer
  def send_xls(fecha_comienzo, usuario, file, fecha_termino)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/alerts/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")
    mail to: usuario.email , subject: "Export ALert to Excel is ready: #{@file.to_s}"
  end

  #alertc y niv_alert
  def send_level_xls_es(fecha_comienzo, usuario_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @user = usuario_log #Esta Variable se usa en la vita del Correo
    @path_file = path_file
    @name_file = name_file
    @fin = fecha_termino
    attachments[@name_file] = File.read("#{@path_file}")

    mail to: @user.email , subject: "Exportación de Filtro Niveles de Alertamiento a Excel lista: #{@name_file}"
  end
  #alertc y niv_alert
  def send_level_xls(fecha_comienzo, usuario_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @user = usuario_log #Esta Variable se usa en la vita del Correo
    @path_file = path_file
    @name_file = name_file
    @fin = fecha_termino
    attachments[@name_file] = File.read("#{@path_file}")

    mail to: @user.email , subject: "Export of Filter in Alerting Levels to Excel is ready: #{@name_file}"
  end
end
