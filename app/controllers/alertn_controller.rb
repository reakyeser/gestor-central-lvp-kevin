class AlertnController < ApplicationController
  # before_action :set_alertn, only: [:show, :edit, :update, :destroy]

  # GET /alertn
  # GET /alertn.json
  @@byNewTran = "New Alert Transaccional"
  @@byNewPer = "New Alert Perfiles"


  #Asi estan en Liver
  # @@byNewTran = "New Alert Transaccional"
  # @@byNewPer = "New Alert Perfiles"

  def index
    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per_n_tran = Permission.where("view_name = 'New Alert Transaccional' and profile_id = ?", @cu)

        @per_n_tran.each do |permisos|
          @leerNewTran = permisos.leer

          if permisos.view_name == @@byNewTran

            @@crearNewTran = permisos.crear
            @@editarNewTran = permisos.editar
            @@leerNewTran = permisos.leer
            @@eliminarNewTran = permisos.eliminar

            @crearNewTran = permisos.crear
            @editarNewTran = permisos.editar
            @leerNewTran = permisos.leer
            @eliminarNewTran = permisos.eliminar
          end
        end

        @per_n_per = Permission.where("view_name = 'New Alert Perfiles' and profile_id = ?", @cu)

        @per_n_per.each do |permisos|
          @leerNewPer = permisos.leer

          if permisos.view_name == @@byNewPer

            @@crearNewPer = permisos.crear
            @@editarNewPer = permisos.editar
            @@leerNewPer = permisos.leer
            @@eliminarNewPer = permisos.eliminar

            @crearNewPer = permisos.crear
            @editarNewPer = permisos.editar
            @leerNewPer = permisos.leer
            @eliminarNewPer = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if (@@leerNewPer == 2 || @@leerNewTran == 2)

            @bitaAlertasxAtender = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
            @bitaAlertasxAtender = Tbitacora.where(:IdUsuario => 'N/A')

            @alert_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
            @alert_Per = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /alertn/1
  # GET /alertn/1.json
  def show
    if current_user.habilitado == 0
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  # GET /alertn/new
  def new
    if current_user.habilitado == 0
      @alertn = Alertn.new
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  # GET /alertn/1/edit
  def edit
    if current_user.habilitado == 0
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end

  end

  # POST /alertn
  # POST /alertn.json
  def create
    @alertn = Alertn.new(alertn_params)

    respond_to do |format|
      if @alertn.save
        format.html {redirect_to @alertn, notice: 'Alertn was successfully created.'}
        format.json {render :show, status: :created, location: @alertn}
      else
        format.html {render :new}
        format.json {render json: @alertn.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /alertn/1
  # PATCH/PUT /alertn/1.json
  def update
    respond_to do |format|
      if @alertn.update(alertn_params)
        format.html {redirect_to @alertn, notice: 'Alertn was successfully updated.'}
        format.json {render :show, status: :ok, location: @alertn}
      else
        format.html {render :edit}
        format.json {render json: @alertn.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /alertn/1
  # DELETE /alertn/1.json
  def destroy

    if current_user.habilitado == 0
      @alertn.destroy
      respond_to do |format|
        format.html {redirect_to alertns_url, notice: 'Alertn was successfully destroyed.'}
        format.json {head :no_content}
      end
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  def alertTran

    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per_n_tran = Permission.where("view_name = 'New Alert Transaccional' and profile_id = ?", @cu)

        @per_n_tran.each do |permisos|
          @crearNewTran = permisos.crear
          @editarNewTran = permisos.editar
          @leerNewTran = permisos.leer

          if permisos.view_name == @@byNewTran

            @@crearNewTran = permisos.crear
            @@editarNewTran = permisos.editar
            @@leerNewTran = permisos.leer
            @@eliminarNewTran = permisos.eliminar

            @crearNewTran = permisos.crear
            @editarNewTran = permisos.editar
            @leerNewTran = permisos.leer
            @eliminarNewTran = permisos.eliminar
          end
        end

        if current_user.habilitado == 0

          if (@@leerNewTran == 2)

            st = Testadosxalerta.select(:IdEstado, :falso_positivo).all
            gon.estados = st
            gon.false = true

            ## Todos los filtros
            @hash_filtros = {}
            filtros = TxFilters.all
            filtros.each do |fil|
              @hash_filtros.store(fil.Id, "#{fil.Id}-#{fil.Description}")
            end

            #Todos los estados
            @hash_states = {}
            states = Testadosxalerta.all
            states.each do |sta|
              @hash_states.store(sta.IdEstado, "#{sta.IdEstado}-#{sta.Descripcion}")
            end

            #Todos los usuarios
            @hash_users = {}
            users = User.all
            users.each do |usr|
              @hash_users.store(usr.id, "#{usr.name} #{usr.last_name}")
            end


            ### Segimiento de alerta
            @idObseTran = params[:id]

            if params[:id]

              @id_consulta = params[:id]
              @alerta_Tran = Tbitacora.where(:Id => @id_consulta).first
              @obser = Tbitaobse.where(:IdAlerta => params[:id]).order("Id Desc")
              @nuevo = true

              regresar = Tatendiendo.where("IdTran = #{@alerta_Tran.IdTran}")


              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10) #Historial de observaciones Vista
              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc") #Historial de observaciones Vista
              # @@Observaciones = @observaciones
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc") #Historial de observaciones Excel



              #Esto Agregar los datos del usuario y alerta IdTran =
              @atendiendo = Tatendiendo.new
              @atendiendo.IdUsuario = current_user.id
              @atendiendo.IdTran = @alerta_Tran.IdTran
              @atendiendo.save

              ### Cuando guarda la observacion de una nueva alerta / Está no es la modal
            elsif params[:observaciones]
              @id_tran = params[:id_tran]
              @id_consulta = @id_tran
              @alerta_Tran = Tbitacora.where(:Id => @id_tran).first
              @obser = Tbitaobse.where(:IdAlerta => params[:id_tran]).order("Id Desc")
              @nuevo = true

              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              # @@Observaciones = @observaciones
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc")

              ### Cuando entra a atender una nueva alerta
            else
              #Verificar si el usuario ya tenía cuentas atendiendo actualmente
              @cuentas_ocupando = Tatendiendo.where(:IdUsuario => current_user.id)

              # Si tiene cuentas atendiendo se elimina de Tatendiendo la cuenta.
              if @cuentas_ocupando.present? && !@cuentas_ocupando.nil?
                @cuentas_ocupando.each do |cta|
                  cta.destroy
                end
              end

              #Verificar si hay cuentas que se están atendiendo de otros usuarios
              @cuentas_ocupadas = Tatendiendo.all
              query = ""

              ### Se hace un query donde no se incluyan las cuentas que se estan atendiendo por otros usuarios
              if @cuentas_ocupadas.present? && !@cuentas_ocupadas.nil?
                @cuentas_ocupadas.each do |cta|
                  if query == ""
                    # query = query + "#{t('config.main')} != '#{cta.IdTran}'"
                    query = query + "IdTran != '#{cta.IdTran}'"
                  else
                    # query = query + " and #{t('config.main')} != '#{cta.IdTran}'"
                    query = query + " and IdTran != '#{cta.IdTran}'"
                  end
                end
              end

              # Verificar las prioridades del usuario
              @prioridades_usuario = Txfilterxusr.where(:IdUsuario => current_user.id)
              if @prioridades_usuario.present? && !@prioridades_usuario.nil?
                @prioridades_usuario.each do |prio|
                  if query == ""
                    query = query + "IdFiltro = #{prio.Txfiltro_id}"
                  else
                    query = query + " or IdFiltro = #{prio.Txfiltro_id}"
                  end
                end
              end

              # Asignación de alerta de acuerdo a atencion y a prioridad
              puts "Soy query: #{query}"
              if query != ""
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).where(:IdUsuario => 'N/A').where(query).order(:Id).last
                if !@alerta_Tran.present? && @alerta_Tran.nil?
                  @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).where(:IdUsuario => 'N/A').order(:Id).last
                end
              else
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).where(:IdUsuario => 'N/A').order(:Id).last
              end
              puts "Soy @alerta_Tran: #{@alerta_Tran.present?}"

              #Agregar el registro de que el usuario está atendiendo una cuenta
              puts "IDAlertTran: #{@alerta_Tran.IdTran}"

              @atendiendo = Tatendiendo.new
              @atendiendo.IdUsuario = current_user.id
              @atendiendo.IdTran = @alerta_Tran.IdTran
              @atendiendo.save

              puts "Soy @atendiendo: #{@atendiendo.present?}"

              @nuevo = true
            end

            ### Termina validaciones si es un seguimiento o nueva alerta

            @@IdAlerta = @alerta_Tran.Id
            @IdAlerta = @alerta_Tran.Id
            @alerta_Tran.IdUsuario = current_user.id
            @alerta_Tran.save


            # Campo Pibote
            @IdTran = @alerta_Tran.IdTran

            #Verifica si existe este NumTarj en White List
            @fechaHoy = Time.now.strftime("%Y-%m-%d")
            @estaEnWL =  WhiteList.find_by_IdTran_and_Fecha(@IdTran,@fechaHoy)

            # Historial de Alertas Vista
            @ConsultaHistTbit = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc").page(params[:page]).per(10)
            # Historial de Alertas Excel
            @ConsultaHistTbit_xls = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc")


            #Se inserta referencia en base y se almacena columnas para TTs
            base_utilizada = ActiveRecord::Base.connection.adapter_name.downcase

            base_utilizada = base_utilizada.to_sym

            case base_utilizada
            when :mysql
              sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
              sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            when :mysql2
              sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
              sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            when :sqlite
              sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
              sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            when :sqlserver
              sql_exist = 'SELECT TOP(1) * FROM %{reference_table} WHERE idAlerta = %{idAlerta}'
              sql_insert = 'INSERT INTO [%{reference_table}] ([idAlerta], [idCampo], [idProducto], [campo], [valor], [created_at], [updated_at]) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            end

            reference_table = 'referenciadealerta'
            @cols_tt = ""

            sql_exist_q = sql_exist % {reference_table: reference_table, idAlerta: @IdAlerta}
            # puts (sql_exist_q)
            Timeout::timeout(0) {@alerta_existente = ActiveRecord::Base::connection.exec_query(sql_exist_q)}

            if !@alerta_existente.nil? # No existe la referencia de la alerta
              @camposref = @alerta_Tran.Referencia.split(">")

              @arrCabecera_excel = []

              @camposref.each do |ref|
                @encabezado = ref.split("=").first
                @idCampo = @encabezado[1..3]
                @campo = "'#{@encabezado.from(5)}'"
                @valor = "'#{ref.split("=").second}'"
                @createDate = "'#{Time.now.to_datetime.strftime("%F %T")}'"


                @arrCabecera_excel.push(@campo)

                #Insertar registro
                sql_insert_q = sql_insert % {reference_table: reference_table, idAlerta: @IdAlerta, idCampo: @idCampo, idProducto: 1, campo: "#{@campo}", valor: "#{@valor}", created_at: "#{@createDate}", updated_at: "#{@createDate}"}
                # puts(sql_insert_q)
                Timeout::timeout(0) {@inserted = ActiveRecord::Base::connection.exec_query(sql_insert_q)}

                if @cols_tt == ""
                  @cols_tt = @cols_tt + @campo
                else
                  @cols_tt = @cols_tt + ", " + @campo
                end
              end
            end

            # ---------- Observaciones

            # Thread Jose Luis 11/12/2018 Solo insercion de Observaciones
            @atn_usr = Tatendiendo.find_by(:IdUsuario => current_user.id, :IdTran => @alerta_Tran.IdTran) #@alerta_Tran.IdTran => esto siempre es el número de tarjeta

            puts "Cual es la alerta que esta atendiendo::: #{@atn_usr.present?} && #{!@atn_usr.nil?}"

            # if @atn_usr.present? && !@atn_usr.nil?
            #   Thread.new do
            #
            #     begin
            #       ## Este podria ser de la modal
            #       if params[:observaciones]
            #
            #         puts "Entre para agregar observaciones::::::::::::::::"
            #
            #         @ne = Tbitaobse.new
            #         @ne.IdAlerta = @@IdAlerta
            #         @ne.Fecha = Time.now.to_date
            #         tiempo = Time.now
            #         @tiempo = tiempo.strftime("%I:%M:%S")
            #         @ne.Hora = @tiempo
            #         @ne.IdEstado = params[:estado]
            #         @ne.IdUsuarioAtencion = current_user.id
            #         @ne.Observaciones = params[:observaciones]
            #         @ne.falso_positivo = params[:fraud]
            #         #@ne.save
            #
            #         respond_to do |format|
            #           if @ne.save
            #             format.json { render :json =>  {:message => "success"} }
            #           else
            #             format.json { render :json =>  {:message => "error"} }
            #           end
            #         end
            #
            #         @estado_ant = @alerta_Tran.IdEstado
            #
            #         @alerta_Tran.IdEstado = params[:estado]
            #         @alerta_Tran.FalsoPositivo = params[:fraud]
            #         @alerta_Tran.save
            #
            #         @estado_act = @alerta_Tran.IdEstado
            #
            #         if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
            #           @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))
            #
            #           if @atendidasxhoy.present? && !@atendidasxhoy.nil?
            #             @cantidad = @atendidasxhoy.cantidad
            #             @atendidasxhoy.cantidad = @cantidad + 1
            #
            #
            #             respond_to do |format|
            #               if @atendidasxhoy.save
            #                 format.json { render :json =>  {:message => "success"} }
            #               else
            #                 format.json { render :json =>  {:message => "error"} }
            #               end
            #             end
            #
            #
            #           else
            #             @nueva_atendida = Tatendidasxhoy.new
            #             @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
            #             @nueva_atendida.idProducto = 1
            #             @nueva_atendida.cantidad = 1
            #             respond_to do |format|
            #               if @atendidasxhoy.save
            #                 format.json { render :json =>  {:message => "success"} }
            #               else
            #                 format.json { render :json =>  {:message => "error"} }
            #               end
            #             end
            #           end
            #
            #         end
            #
            #         @apply = params[:apply]
            #
            #         if @apply == "true"
            #           puts "Agregar multiple Observacion ::::::::::::::::"
            #           @historico_alert = params[:historico]
            #           if !@historico_alert.nil? && @historico_alert != ""
            #             historico_al = @historico_alert.split(',')
            #
            #             puts "Arreglo de id a los que voy agregar #{historico_al}"
            #
            #             loop do
            #
            #               @ne = Tbitaobse.new
            #               @ne.IdAlerta = historico_al[0]
            #               @ne.Fecha = Time.now.to_date
            #               tiempo = Time.now
            #               @tiempo = tiempo.strftime("%I:%M:%S")
            #               @ne.Hora = @tiempo
            #               @ne.IdEstado = params[:estado]
            #               @ne.IdUsuarioAtencion = current_user.id
            #               @ne.Observaciones = params[:observaciones]
            #               @ne.falso_positivo = params[:fraud]
            #               @ne.save
            #
            #               @alerta_observacion = Tbitacora.find(historico_al[0])
            #
            #               @estado_ant = @alerta_observacion.IdEstado
            #
            #               if @alerta_observacion
            #                 @alerta_observacion.IdEstado = params[:estado]
            #                 @alerta_observacion.IdUsuario = current_user.id
            #                 @alerta_observacion.FalsoPositivo = params[:fraud]
            #                 @alerta_observacion.save
            #               end
            #
            #               @estado_act = @alerta_observacion.IdEstado
            #
            #               if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
            #                 @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))
            #
            #                 if @atendidasxhoy.present? && !@atendidasxhoy.nil?
            #                   @cantidad = @atendidasxhoy.cantidad
            #                   @atendidasxhoy.cantidad = @cantidad + 1
            #                   @atendidasxhoy.save
            #                 else
            #                   @nueva_atendida = Tatendidasxhoy.new
            #                   @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
            #                   @nueva_atendida.idProducto = 1
            #                   @nueva_atendida.cantidad = 1
            #                   @nueva_atendida.save
            #                 end
            #
            #               end
            #
            #               historico_al.delete_at(0)
            #               break if historico_al.length == 0
            #             end
            #           end
            #         end
            #
            #       end
            #
            #     rescue => e
            #       puts 'Archivo: log/error_denuevaAlerta.log'
            #       logger = Logger.new("log/error_denuevaAlerta.log")
            #       logger.error('----------------------------------------------------------')
            #       logger.error(e)
            #     end
            #   end
            # end
            # Thread Jose Luis 11/12/2018 Solo insercion de Observaciones
            # ----------  Observaciones

            # ----------  Historial de Transacciones


            @campos = @cols_tt.gsub('"', '').split(',')

            @f1 = 10.days.ago.strftime('%Y%m%d')
            @f2 = Time.now.strftime('%Y%m%d')

            datosScript = Array.new
            @sess = '0101'
            @tabla1 = 'TT' + @f1.to_s + "0101"
            @tabla2 = 'TT' + @f2.to_s + "0101"

            nameDatabase = Rails.configuration.database_configuration[Rails.env]["database"]
            @tablas = ActiveRecord::Base.connection.exec_query("select table_name as tabla from information_schema.tables where table_catalog='" + nameDatabase + "' and table_name between '" + @tabla1 + "' and '" + @tabla2 + "' and table_name like '%" + @sess.to_s + "' order by tabla ASC")

            @campo = Hash.new
            contador = 0
            @campos = @cols_tt.gsub('"', '').gsub("'", "").split(',')
            @tablas.each do |tablas|
              @t_all = tablas['tabla']
              @campos.each do |campos|
                contador += 1
                begin
                  ActiveRecord::Base.connection.exec_query("SELECT TOP 1 #{campos} from #{@t_all}")
                  # @campo.push(campos)
                  @campo.store( contador, campos)
                rescue => e
                  mensaje = "#{"'este campo no existe' as"} " "#{campos}"
                  msj = mensaje.gsub(/"/, "")
                  # @campo.push(msj)
                  @campo.store( contador, msj)
                end
              end
              contador = 0
            end
            puts "Soy HashCampos: #{@campo.values}"
            @campos = @campo.values.to_s.gsub(/"/, "").gsub(/[\[\]]/, '')
            puts "Soy Resultador: #{@campos}"

            # @campos = @cols_tt.gsub('"', '').gsub("'", "")
            # @cols_ref = @cols_tt.gsub('"', '').gsub("'", "").split(",")
            @tablas.each do |tablas|
              valor = tablas
              @t_all = tablas['tabla']
              @datos = ActiveRecord::Base::connection.exec_query("SELECT FECHA_HORA_KM, #{@campos} from #{@t_all} where #{t('config.main')} = '#{@IdTran}'")
              @datos.each do |datos|
                datosScript.push(datos)
                # puts "#{datosScript}"
              end
            end

            # puts "#{datosScript}"

            @transacciones = datosScript
            # @transacciones = Kaminari.paginate_array(datosScript).page(params[:page]).per(20)
            # ---------- Fin Historial de Transacciones

            @states = Testadosxalerta.all.where.not(:IdEstado => '1')

            if params[:xls_g]
              Thread.new do
                puts 'Comienza hilo para TRANSACCIONAL'

                puts "Array para cabecera:  #{@arrCabecera_excel}"

                create_xls = CreateXls.new
                create_xls.setInfoAlertXls(params[:id], params[:user_id], params[:idioma], @observaciones_xls, @ConsultaHistTbit_xls, @transacciones, @arrCabecera_excel)
                create_xls.alertXls

              end
            end


          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def alertPer

    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per_n_tran = Permission.where("view_name = 'New Alert Transaccional' and profile_id = ?", @cu)

        @per_n_tran.each do |permisos|
          @crearNewTran = permisos.crear
          @editarNewTran = permisos.editar
          @leerNewTran = permisos.leer

          if permisos.view_name == @@byNewTran

            @@crearNewTran = permisos.crear
            @@editarNewTran = permisos.editar
            @@leerNewTran = permisos.leer
            @@eliminarNewTran = permisos.eliminar

            @crearNewTran = permisos.crear
            @editarNewTran = permisos.editar
            @leerNewTran = permisos.leer
            @eliminarNewTran = permisos.eliminar
          end
        end

        if current_user.habilitado == 0

          if (@@leerNewTran == 2)

            st = Testadosxalerta.select(:IdEstado, :falso_positivo).all
            gon.estados = st
            gon.false = true

            ## Todos los filtros
            @hash_filtros = {}
            filtros = TxFilters.all
            filtros.each do |fil|
              @hash_filtros.store(fil.Id, "#{fil.Id}-#{fil.Description}")
            end

            #Todos los estados
            @hash_states = {}
            states = Testadosxalerta.all
            states.each do |sta|
              @hash_states.store(sta.IdEstado, "#{sta.IdEstado}-#{sta.Descripcion}")
            end

            #Todos los usuarios
            @hash_users = {}
            users = User.all
            users.each do |usr|
              @hash_users.store(usr.id, "#{usr.name} #{usr.last_name}")
            end


            ### Segimiento de alerta
            @idObseTran = params[:id]

            if params[:id]

              @id_consulta = params[:id]
              @alerta_Tran = Tbitacora.where(:Id => @id_consulta).first
              @obser = Tbitaobse.where(:IdAlerta => params[:id]).order("Id Desc")
              @nuevo = true

              regresar = Tatendiendo.where("IdTran = #{@alerta_Tran.IdTran}")


              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10) #Historial de observaciones Vista
              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc") #Historial de observaciones Vista
              # @@Observaciones = @observaciones
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc") #Historial de observaciones Excel



              #Esto Agregar los datos del usuario y alerta IdTran =
              @atendiendo = Tatendiendo.new
              @atendiendo.IdUsuario = current_user.id
              @atendiendo.IdTran = @alerta_Tran.IdTran
              @atendiendo.save

              ### Cuando guarda la observacion de una nueva alerta / Está no es la modal
            elsif params[:observaciones]
              @id_tran = params[:id_tran]
              @id_consulta = @id_tran
              @alerta_Tran = Tbitacora.where(:Id => @id_tran).first
              @obser = Tbitaobse.where(:IdAlerta => params[:id_tran]).order("Id Desc")
              @nuevo = true

              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              # @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              # @@Observaciones = @observaciones
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc")

              ### Cuando entra a atender una nueva alerta
            else
              #Verificar si el usuario ya tenía cuentas atendiendo actualmente
              @cuentas_ocupando = Tatendiendo.where(:IdUsuario => current_user.id)

              # Si tiene cuentas atendiendo se elimina de Tatendiendo la cuenta.
              if @cuentas_ocupando.present? && !@cuentas_ocupando.nil?
                @cuentas_ocupando.each do |cta|
                  cta.destroy
                end
              end

              #Verificar si hay cuentas que se están atendiendo de otros usuarios
              @cuentas_ocupadas = Tatendiendo.all
              query = ""

              ### Se hace un query donde no se incluyan las cuentas que se estan atendiendo por otros usuarios
              if @cuentas_ocupadas.present? && !@cuentas_ocupadas.nil?
                @cuentas_ocupadas.each do |cta|
                  if query == ""
                    # query = query + "#{t('config.main')} != '#{cta.IdTran}'"
                    query = query + "IdTran != '#{cta.IdTran}'"
                  else
                    # query = query + " and #{t('config.main')} != '#{cta.IdTran}'"
                    query = query + " and IdTran != '#{cta.IdTran}'"
                  end
                end
              end

              # Verificar las prioridades del usuario
              @prioridades_usuario = Txfilterxusr.where(:IdUsuario => current_user.id)
              if @prioridades_usuario.present? && !@prioridades_usuario.nil?
                @prioridades_usuario.each do |prio|
                  if query == ""
                    query = query + "IdFiltro = #{prio.Txfiltro_id}"
                  else
                    query = query + " or IdFiltro = #{prio.Txfiltro_id}"
                  end
                end
              end

              # Asignación de alerta de acuerdo a atencion y a prioridad
              puts "Soy query: #{query}"
              if query != ""
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).where(:IdUsuario => 'N/A').where(query).order(:Id).last
                if !@alerta_Tran.present? && @alerta_Tran.nil?
                  @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).where(:IdUsuario => 'N/A').order(:Id).last
                end
              else
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).where(:IdUsuario => 'N/A').order(:Id).last
              end
              puts "Soy @alerta_Tran: #{@alerta_Tran.present?}"

              #Agregar el registro de que el usuario está atendiendo una cuenta
              puts "IDAlertTran: #{@alerta_Tran.IdTran}"

              @atendiendo = Tatendiendo.new
              @atendiendo.IdUsuario = current_user.id
              @atendiendo.IdTran = @alerta_Tran.IdTran
              @atendiendo.save

              puts "Soy @atendiendo: #{@atendiendo.present?}"

              @nuevo = true
            end

            ### Termina validaciones si es un seguimiento o nueva alerta

            @@IdAlerta = @alerta_Tran.Id
            @IdAlerta = @alerta_Tran.Id
            @alerta_Tran.IdUsuario = current_user.id
            @alerta_Tran.save


            # Campo Pibote
            @IdTran = @alerta_Tran.IdTran


            # Historial de Alertas Vista
            @ConsultaHistTbit = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 2).order("Fecha Desc").order("Hora Desc").page(params[:page]).per(10)
            # Historial de Alertas Excel
            @ConsultaHistTbit_xls = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 2).order("Fecha Desc").order("Hora Desc")


            #Se inserta referencia en base y se almacena columnas para TTs
            base_utilizada = ActiveRecord::Base.connection.adapter_name.downcase

            base_utilizada = base_utilizada.to_sym

            case base_utilizada
            when :mysql
              sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
              sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            when :mysql2
              sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
              sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            when :sqlite
              sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
              sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            when :sqlserver
              sql_exist = 'SELECT TOP(1) * FROM %{reference_table} WHERE idAlerta = %{idAlerta}'
              sql_insert = 'INSERT INTO [%{reference_table}] ([idAlerta], [idCampo], [idProducto], [campo], [valor], [created_at], [updated_at]) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            end

            reference_table = 'referenciadealerta'
            @cols_tt = ""

            sql_exist_q = sql_exist % {reference_table: reference_table, idAlerta: @IdAlerta}
            # puts (sql_exist_q)
            Timeout::timeout(0) {@alerta_existente = ActiveRecord::Base::connection.exec_query(sql_exist_q)}

            if !@alerta_existente.nil? # No existe la referencia de la alerta
              @camposref = @alerta_Tran.Referencia.split(">")

              @arrCabecera_excel = []

              @camposref.each do |ref|
                @encabezado = ref.split("=").first
                @idCampo = @encabezado[1..3]
                @campo = "'#{@encabezado.from(5)}'"
                @valor = "'#{ref.split("=").second}'"
                @createDate = "'#{Time.now.to_datetime.strftime("%F %T")}'"


                @arrCabecera_excel.push(@campo)

                #Insertar registro
                sql_insert_q = sql_insert % {reference_table: reference_table, idAlerta: @IdAlerta, idCampo: @idCampo, idProducto: 2, campo: "#{@campo}", valor: "#{@valor}", created_at: "#{@createDate}", updated_at: "#{@createDate}"}
                # puts(sql_insert_q)
                Timeout::timeout(0) {@inserted = ActiveRecord::Base::connection.exec_query(sql_insert_q)}

                if @cols_tt == ""
                  @cols_tt = @cols_tt + @campo
                else
                  @cols_tt = @cols_tt + ", " + @campo
                end
              end
            end

            # ---------- Observaciones

            # Thread Jose Luis 11/12/2018 Solo insercion de Observaciones
            @atn_usr = Tatendiendo.find_by(:IdUsuario => current_user.id, :IdTran => @alerta_Tran.IdTran) #@alerta_Tran.IdTran => esto siempre es el número de tarjeta

            puts "Cual es la alerta que esta atendiendo::: #{@atn_usr.present?} && #{!@atn_usr.nil?}"

            # if @atn_usr.present? && !@atn_usr.nil?
            #   Thread.new do
            #
            #     begin
            #       ## Este podria ser de la modal
            #       if params[:observaciones]
            #
            #         puts "Entre para agregar observaciones::::::::::::::::"
            #
            #         @ne = Tbitaobse.new
            #         @ne.IdAlerta = @@IdAlerta
            #         @ne.Fecha = Time.now.to_date
            #         tiempo = Time.now
            #         @tiempo = tiempo.strftime("%I:%M:%S")
            #         @ne.Hora = @tiempo
            #         @ne.IdEstado = params[:estado]
            #         @ne.IdUsuarioAtencion = current_user.id
            #         @ne.Observaciones = params[:observaciones]
            #         @ne.falso_positivo = params[:fraud]
            #         #@ne.save
            #
            #         respond_to do |format|
            #           if @ne.save
            #             format.json { render :json =>  {:message => "success"} }
            #           else
            #             format.json { render :json =>  {:message => "error"} }
            #           end
            #         end
            #
            #         @estado_ant = @alerta_Tran.IdEstado
            #
            #         @alerta_Tran.IdEstado = params[:estado]
            #         @alerta_Tran.FalsoPositivo = params[:fraud]
            #         @alerta_Tran.save
            #
            #         @estado_act = @alerta_Tran.IdEstado
            #
            #         if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
            #           @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))
            #
            #           if @atendidasxhoy.present? && !@atendidasxhoy.nil?
            #             @cantidad = @atendidasxhoy.cantidad
            #             @atendidasxhoy.cantidad = @cantidad + 1
            #
            #
            #             respond_to do |format|
            #               if @atendidasxhoy.save
            #                 format.json { render :json =>  {:message => "success"} }
            #               else
            #                 format.json { render :json =>  {:message => "error"} }
            #               end
            #             end
            #
            #
            #           else
            #             @nueva_atendida = Tatendidasxhoy.new
            #             @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
            #             @nueva_atendida.idProducto = 1
            #             @nueva_atendida.cantidad = 1
            #             respond_to do |format|
            #               if @atendidasxhoy.save
            #                 format.json { render :json =>  {:message => "success"} }
            #               else
            #                 format.json { render :json =>  {:message => "error"} }
            #               end
            #             end
            #           end
            #
            #         end
            #
            #         @apply = params[:apply]
            #
            #         if @apply == "true"
            #           puts "Agregar multiple Observacion ::::::::::::::::"
            #           @historico_alert = params[:historico]
            #           if !@historico_alert.nil? && @historico_alert != ""
            #             historico_al = @historico_alert.split(',')
            #
            #             puts "Arreglo de id a los que voy agregar #{historico_al}"
            #
            #             loop do
            #
            #               @ne = Tbitaobse.new
            #               @ne.IdAlerta = historico_al[0]
            #               @ne.Fecha = Time.now.to_date
            #               tiempo = Time.now
            #               @tiempo = tiempo.strftime("%I:%M:%S")
            #               @ne.Hora = @tiempo
            #               @ne.IdEstado = params[:estado]
            #               @ne.IdUsuarioAtencion = current_user.id
            #               @ne.Observaciones = params[:observaciones]
            #               @ne.falso_positivo = params[:fraud]
            #               @ne.save
            #
            #               @alerta_observacion = Tbitacora.find(historico_al[0])
            #
            #               @estado_ant = @alerta_observacion.IdEstado
            #
            #               if @alerta_observacion
            #                 @alerta_observacion.IdEstado = params[:estado]
            #                 @alerta_observacion.IdUsuario = current_user.id
            #                 @alerta_observacion.FalsoPositivo = params[:fraud]
            #                 @alerta_observacion.save
            #               end
            #
            #               @estado_act = @alerta_observacion.IdEstado
            #
            #               if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
            #                 @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))
            #
            #                 if @atendidasxhoy.present? && !@atendidasxhoy.nil?
            #                   @cantidad = @atendidasxhoy.cantidad
            #                   @atendidasxhoy.cantidad = @cantidad + 1
            #                   @atendidasxhoy.save
            #                 else
            #                   @nueva_atendida = Tatendidasxhoy.new
            #                   @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
            #                   @nueva_atendida.idProducto = 1
            #                   @nueva_atendida.cantidad = 1
            #                   @nueva_atendida.save
            #                 end
            #
            #               end
            #
            #               historico_al.delete_at(0)
            #               break if historico_al.length == 0
            #             end
            #           end
            #         end
            #
            #       end
            #
            #     rescue => e
            #       puts 'Archivo: log/error_denuevaAlerta.log'
            #       logger = Logger.new("log/error_denuevaAlerta.log")
            #       logger.error('----------------------------------------------------------')
            #       logger.error(e)
            #     end
            #   end
            # end
            # Thread Jose Luis 11/12/2018 Solo insercion de Observaciones
            # ----------  Observaciones

            # ----------  Historial de Transacciones


            @campos = @cols_tt.gsub('"', '').split(',')

            @f1 = 10.days.ago.strftime('%Y%m%d')
            @f2 = Time.now.strftime('%Y%m%d')

            datosScript = Array.new
            @sess = '0101'
            @tabla1 = 'TT' + @f1.to_s + "0101"
            @tabla2 = 'TT' + @f2.to_s + "0101"

            nameDatabase = Rails.configuration.database_configuration[Rails.env]["database"]
            @tablas = ActiveRecord::Base.connection.exec_query("select table_name as tabla from information_schema.tables where table_catalog='" + nameDatabase + "' and table_name between '" + @tabla1 + "' and '" + @tabla2 + "' and table_name like '%" + @sess.to_s + "' order by tabla ASC")

            @campo = Hash.new
            contador = 0
            @tablas.each do |tablas|
              @t_all = tablas['tabla']
              @cols_tt.each do |campos|
                contador += 1
                begin
                  ActiveRecord::Base.connection.exec_query("SELECT TOP 1 #{campos} from #{@t_all}")
                  # @campo.push(campos)
                  @campo.store( contador, campos)
                rescue => e
                  mensaje = "#{"'este campo no existe' as"} " "#{campos}"
                  msj = mensaje.gsub(/"/, "")
                  # @campo.push(msj)
                  @campo.store( contador, msj)
                end
              end
              contador = 0
            end
            puts "Soy HashCampos: #{@campo.values}"
            @campos = @campo.values.to_s.gsub(/"/, "").gsub(/[\[\]]/, '')
            puts "Soy Resultador: #{@campos}"

            # @campos = @cols_tt.gsub('"', '').gsub("'", "")
            # @cols_ref = @cols_tt.gsub('"', '').gsub("'", "").split(",")
            @tablas.each do |tablas|
              valor = tablas
              @t_all = tablas['tabla']
              @datos = ActiveRecord::Base::connection.exec_query("SELECT FECHA_HORA_KM, #{@campos} from #{@t_all} where #{t('config.main')} = '#{@IdTran}'")
              @datos.each do |datos|
                datosScript.push(datos)
                # puts "#{datosScript}"
              end
            end

            # puts "#{datosScript}"

            @transacciones = datosScript
            # @transacciones = Kaminari.paginate_array(datosScript).page(params[:page]).per(20)
            # ---------- Fin Historial de Transacciones

            @states = Testadosxalerta.all.where.not(:IdEstado => '1')

            if params[:xls_g]
              Thread.new do
                puts 'Comienza hilo para TRANSACCIONAL'

                puts "Array para cabecera:  #{@arrCabecera_excel}"

                create_xls = CreateXls.new
                create_xls.setInfoAlertXls(params[:id], params[:user_id], params[:idioma], @observaciones_xls, @ConsultaHistTbit_xls, @transacciones, @arrCabecera_excel)
                create_xls.alertXls

              end
            end


          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def creacion_observaciones
    puts "Mi historico #{params[:historico]}::::::::::::::::"

    Thread.new do
      begin
        ## Este podria ser de la modal
        if params[:observaciones]

          @alert_Tran = Tbitacora.find_by(:Id => params[:idTbitacora])


          puts "Entre para agregar observaciones::::::::::::::::"

          @ne = Tbitaobse.new
          @ne.IdAlerta = params[:idTbitacora]
          @ne.Fecha = Time.now.to_date
          tiempo = Time.now
          @tiempo = tiempo.strftime("%I:%M:%S")
          @ne.Hora = @tiempo
          @ne.IdEstado = params[:estado]
          @ne.IdUsuarioAtencion = current_user.id
          @ne.Observaciones = params[:observaciones]
          @ne.falso_positivo = params[:fraud]
          @ne.save

          # respond_to do |format|
          #   if @ne.save
          #     format.json {render :json => {:message => "success"}}
          #   else
          #     format.json {render :json => {:message => "error"}}
          #   end
          # end

          ##
          @estado_ant = @alert_Tran.IdEstado

          @alert_Tran.IdEstado = params[:estado]
          @alert_Tran.FalsoPositivo = params[:fraud]
          @alert_Tran.save


          puts "Soy nuevo valor Estado #{@alert_Tran.IdEstado}"

          @estado_act = @alert_Tran.IdEstado

          ##Actualiza valores de los counter de atendidas
          if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
            @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))

            if @atendidasxhoy.present? && !@atendidasxhoy.nil?
              @cantidad = @atendidasxhoy.cantidad
              @atendidasxhoy.cantidad = @cantidad + 1
              @atendidasxhoy.save
            else
              @nueva_atendida = Tatendidasxhoy.new
              @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
              @nueva_atendida.idProducto = 1
              @nueva_atendida.cantidad = 1
              @atendidasxhoy.save

            end

          end

          @apply = params[:apply]

          if @apply == "true"
            puts "Agregar multiple Observacion #{params[:historico]}::::::::::::::::"
            @historico_alert = params[:historico]
            if !@historico_alert.nil? && @historico_alert != ""
              historico_al = @historico_alert.split(',')

              puts "Arreglo de id a los que voy agregar #{historico_al}"

              historico_al.each do |histo|
                @ne = Tbitaobse.new
                @ne.IdAlerta = histo
                @ne.Fecha = Time.now.to_date
                tiempo = Time.now
                @tiempo = tiempo.strftime("%I:%M:%S")
                @ne.Hora = @tiempo
                @ne.IdEstado = params[:estado]
                @ne.IdUsuarioAtencion = current_user.id
                @ne.Observaciones = params[:observaciones]
                @ne.falso_positivo = params[:fraud]
                @ne.save

                @alerta_observacion = Tbitacora.find(histo)

                @estado_ant = @alerta_observacion.IdEstado
                if @alerta_observacion
                  @alerta_observacion.IdEstado = params[:estado]
                  @alerta_observacion.IdUsuario = current_user.id
                  @alerta_observacion.FalsoPositivo = params[:fraud]
                  @alerta_observacion.save
                end

                @estado_act = @alerta_observacion.IdEstado

                if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
                  @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))

                  if @atendidasxhoy.present? && !@atendidasxhoy.nil?
                    @cantidad = @atendidasxhoy.cantidad
                    @atendidasxhoy.cantidad = @cantidad + 1
                    @atendidasxhoy.save
                  else
                    @nueva_atendida = Tatendidasxhoy.new
                    @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
                    @nueva_atendida.idProducto = 1
                    @nueva_atendida.cantidad = 1
                    @nueva_atendida.save
                  end

                end
              end

            end
          end

        end

      rescue => e
        puts 'Archivo: log/error_creandoObservacion.log'
        logger = Logger.new("log/error_creandoObservacion.log")
        logger.error('----------------------------------------------------------')
        logger.error(e)
      end
    end
  end

  def observaciones
    # begin

    begin
      @obser = Tbitaobse.where(:IdAlerta => params[:idTbitacora]).order("Id Desc")
      render :partial =>  "observaciones"

    rescue => e
      puts 'Archivo: log/error_RefreshObservacion.log'
      logger = Logger.new("log/error_RefreshObservacion.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
    # rescue => e
    #
    # end
  end

  def historialAlertas

    if params[:id].present? && params[:IdTran].present?
      @IdAlerta = params[:id]
      @IdTran = params[:IdTran]

      # Permisos para tbitaobse
      @editarNewTran = 4
      @editarNewPer = 4

      @ConsultaHistTbit = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc").page(params[:page]).per(10)
      @ConsultaHistTbit_xls = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc")

    end


    render partial: 'alertn/historialAlertas'
  end


  def bloqueo_de_cuenta
    @tbitacora = Tbitacora.find(params[:idt])
    @bloqueo = KonversaAccount.new;
    @msgg = @bloqueo.unblockAccount(params[:idt])

    if @msgg == true
      @tbitacora.update_attribute(:bloqueo_de_cuenta, 0)
      return true
    else
      # format.json {render :json => {:message => @msgg}}
      respond_to do |format|
        format.text { render :text => @msgg }
      end
      return false
    end

    # @tbitacora.save

  end


  def agregar_a_lista_blanca

    @IdTran = params[:IdTran]
    @hoyFecha = Time.now.strftime("%Y-%m-%d")
    @existe = WhiteList.find_by_IdTran_and_Fecha(@IdTran, @hoyFecha)

    @id_user = current_user.id
    @hoy = Time.now
    @fecha = @hoy.strftime("%Y-%m-%d")
    @hora = @hoy.strftime("%H:%M:%S.%L")

    if !@existe.present?
      @listaBlanca = WhiteList.new
      @listaBlanca.IdUsuario = @id_user
      @listaBlanca.IdTran = @IdTran
      @listaBlanca.Fecha = @fecha
      @listaBlanca.Estado = 1
      @listaBlanca.Hora = @hora

      if @listaBlanca.save
        @bitaWL = BitacoraWhiteList.new
        @bitaWL.IdUsuario = @id_user
        @bitaWL.Estado = 1
        @bitaWL.IdLista = @listaBlanca.id
        @bitaWL.Hora = @hora
        @bitaWL.Fecha = @fecha
        @bitaWL.save
      end
    else
      @existe.IdUsuario = @id_user
      @existe.Fecha = @fecha
      @existe.Estado = 1
      @existe.Hora = @hora

      if @existe.save
        @bitaWL = BitacoraWhiteList.new
        @bitaWL.IdUsuario = @id_user
        @bitaWL.Estado = 1
        @bitaWL.IdLista = @existe.id
        @bitaWL.Hora = @hora
        @bitaWL.Fecha = @fecha
        @bitaWL.save
      end
    end

  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_alertn
    @alertn = Alertn.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def alertn_params
    params.fetch(:alertn, {})
  end
end