class UserPriorityController < ApplicationController

  @@byPriorityTran = "User Attention Priority Transaccional"
  @@byPriorityPer = "User Attention Priority Perfiles"

  def index
    if current_user
      @id_usuario = current_user.id
      @id_aplicacion = 25
      @idTipoReg = 11 #Index
      @fecha = Time.now.strftime("%F")
      @hora = Time.now.strftime("%k:%M:%S.00000")
      @men = "User Attention Priority Index"

      @tbit = Tbitacora.new
      @tbit.IdUsuario = @id_usuario
      @tbit.IdAplicacion = @id_aplicacion
      @tbit.IdTipoReg = @idTipoReg #Index
      @tbit.Fecha = @fecha
      @tbit.Hora = @hora
      @tbit.Mensaje = @men
      @tbit.save
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'User Attention Priority Transaccional' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearPrioridad = permisos.crear
          @editarPrioridad = permisos.editar
          @leerPrioridad = permisos.leer
          @eliminarPrioridad = permisos.eliminar

          if permisos.view_name == @@byPriorityTran

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        @perPerf = Permission.where("view_name = 'User Attention Priority Perfiles' and profile_id = ?", @cu)

        @perPerf.each do |permisos|
          @uno = permisos.view_name
          @crearPrioridadPer = permisos.crear
          @editarPrioridadPer = permisos.editar
          @leerPrioridadPer = permisos.leer
          @eliminarPrioridadPer = permisos.eliminar

          if permisos.view_name == @@byPriorityPer

            @@crearPer = permisos.crear
            @@editarPer = permisos.editar
            @@leerPer = permisos.leer
            @@eliminarPer = permisos.eliminar

            @crearPer = permisos.crear
            @editarPer = permisos.editar
            @leerPer = permisos.leer
            @eliminarPer = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if (((@@editar == 4) || (@@leer == 2)) || ((@@editarPer == 4) || (@@leerPer == 2)))

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def sessionsTran
    if current_user
      @id_usuario = current_user.id
      @id_aplicacion = 25
      @idTipoReg = 11 #Index
      @fecha = Time.now.strftime("%F")
      @hora = Time.now.strftime("%k:%M:%S.00000")
      @men = "User Attention Priority Key Monitor Transaccional Index"


      @tbit = Tbitacora.new
      @tbit.IdUsuario = @id_usuario
      @tbit.IdAplicacion = @id_aplicacion
      @tbit.IdTipoReg = @idTipoReg #Index
      @tbit.Fecha = @fecha
      @tbit.Hora = @hora
      @tbit.Mensaje = @men
      @tbit.save
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'User Attention Priority Transaccional' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearPrioridad = permisos.crear
          @editarPrioridad = permisos.editar
          @leerPrioridad = permisos.leer
          @eliminarPrioridad = permisos.eliminar

          if permisos.view_name == @@byPriorityTran

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if (((@@editar == 4) || (@@leer == 2)))
            @sesiones_filtros

            @sesiones = TxSessions.all

            sesiones_filtros = Array.new
            @sesiones.each do |ses|
              @filtro = TxFilters.where("Session_id = ?", ses.Id).first

              valor = {sesion: "", filtro: ""}
              valor[:sesion] = ses.Description
              valor[:filtro] = @filtro.Description
              valor[:idSesion] = ses.Id
              valor[:idFiltro] = @filtro.Id

              sesiones_filtros.push(valor)
            end

            @sesiones_filtros = Kaminari.paginate_array(sesiones_filtros).page(params[:page]).per(20)
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def groupsPer
    if current_user
##-- se guarda histórico
      @id_usuario = current_user.id
      @id_aplicacion = 25
      @idTipoReg = 11 #Index
      @fecha = Time.now.strftime("%F")
      @hora = Time.now.strftime("%k:%M:%S.00000")
      @men = "Groups Permissions"

      @tbit = Tbitacora.new
      @tbit.IdUsuario = @id_usuario
      @tbit.IdAplicacion = @id_aplicacion
      @tbit.IdTipoReg = @idTipoReg #Index
      @tbit.Fecha = @fecha
      @tbit.Hora = @hora
      @tbit.Mensaje = @men
      @tbit.save
##-- se guarda histórico
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'User Attention Priority Perfiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearPrioridad = permisos.crear
          @editarPrioridad = permisos.editar
          @leerPrioridad = permisos.leer
          @eliminarPrioridad = permisos.eliminar

          if permisos.view_name == @@byPriorityPer

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if (((@@editar == 4) || (@@leer == 2)))
            @perfiles = Tperfiles.all.page(params[:page]).per(20)


            # @sesiones_filtros
            #
            # @sesiones = tx_sessions.all
            #
            # sesiones_filtros = Array.new
            # @sesiones.each do |ses|
            #   @filtro = TxFilters.where("Session_id = ?", ses.Id).first
            #
            #   valor = {sesion: "", filtro: ""}
            #   valor[:sesion] = ses.Description
            #   valor[:filtro] = @filtro.Description
            #   valor[:idSesion] = ses.Id
            #   valor[:idFiltro] = @filtro.Id
            #
            #   sesiones_filtros.push(valor)
            # end
            #
            # @sesiones_filtros = Kaminari.paginate_array(sesiones_filtros).page(params[:page]).per(20)
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def assignPriority
    if current_user
      @id_usuario = current_user.id
      @id_aplicacion = 25
      @idTipoReg = 9
      @fecha = Time.now.strftime("%F")
      @hora = Time.now.strftime("%k:%M:%S.00000")
      @men = "Assign Priorities Key Monitor Transaccional"


      @tbit = Tbitacora.new
      @tbit.IdUsuario = @id_usuario
      @tbit.IdAplicacion = @id_aplicacion
      @tbit.IdTipoReg = @idTipoReg
      @tbit.Fecha = @fecha
      @tbit.Hora = @hora
      @tbit.Mensaje = @men
      @tbit.save
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'User Attention Priority Transaccional' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearPrioridad = permisos.crear
          @editarPrioridad = permisos.editar
          @leerPrioridad = permisos.leer
          @eliminarPrioridad = permisos.eliminar

          if permisos.view_name == @@byPriorityTran

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end
        end

        if current_user.habilitado == 0

          if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

            # Formulario para cambiar analista
            if params[:form_change]
              @idPriority = params[:priorityId]
              @user_change = params[:users_priority]

              @priority = Txfilterxusr.find_by(:Id => @idPriority)
              @priority.IdUsuario = @user_change
              @priority.save
            end

            #Formulario para agregar una prioridad
            if params[:form_priority]
              @level_priority = params[:priority]
              @user_add = params[:users_add_priority]
              @filter = params[:idf]

              @priority = Txfilterxusr.new
              @priority.Txfiltro_id = @filter
              @priority.Prioridad = @level_priority
              @priority.IdUsuario = @user_add
              @priority.save
            end

            #Eliminación de prioridades
            if params[:delete]
              @idPriority = params[:priorityId]
              @filter = params[:idf]

              @priority = Txfilterxusr.find_by(:Id => @idPriority)
              @priority.destroy

              @priorities = Txfilterxusr.where(:Txfiltro_id => @filter).where.not(:Id => @idPriority).order(:Prioridad)
              @count = 0
              @priorities.each do |p|
                @count += 1
                p.Prioridad = @count
                p.save
              end
            end

            # Variables para la pagina
            @idSesion = params[:ids]
            @idFiltro = params[:idf]

            @sesion = TxSessions.find_by(:Id => @idSesion)
            @filtro = TxFilters.find_by(:Id => @idFiltro)

            @prioridades = Txfilterxusr.where(:Txfiltro_id => @idFiltro)
            @cant_priority = @prioridades.count()
            @sig_priority = @cant_priority + 1
            @users = User.all
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def assignPriorityPer
    if current_user
      @id_usuario = current_user.id
      @id_aplicacion = 25
      @idTipoReg = 9
      @fecha = Time.now.strftime("%F")
      @hora = Time.now.strftime("%k:%M:%S.00000")
      @men = "Assign Priorities Key Monitor (P)"


      @tbit = Tbitacora.new
      @tbit.IdUsuario = @id_usuario
      @tbit.IdAplicacion = @id_aplicacion
      @tbit.IdTipoReg = @idTipoReg
      @tbit.Fecha = @fecha
      @tbit.Hora = @hora
      @tbit.Mensaje = @men
      @tbit.save
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'User Attention Priority Perfiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearPrioridad = permisos.crear
          @editarPrioridad = permisos.editar
          @leerPrioridad = permisos.leer
          @eliminarPrioridad = permisos.eliminar

          if permisos.view_name == @@byPriorityPer

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end
        end

        if current_user.habilitado == 0

          if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

            # Formulario para cambiar analista
            if params[:form_change]
              @idGrupo = params[:idg]
              @idPerfil = params[:idp]
              @idPriority = params[:priorityId]
              @user_change = params[:users_priority]

              # @priority = Talertaxusr.find_by(:IdGrupo => @idGrupo.to_i, :IdPerfil => @idPerfil.to_i, :Prioridad => @idPriority.to_i)
              # @priority.destroy
              # #@find = @priority.IdUsuario
              # @priority = Talertaxusr.new
              # @priority.IdGrupo = @idGrupo
              # @priority.IdPerfil = @idPerfil
              # @priority.IdAlerta = 1
              # @priority.Prioridad = @idPriority
              # @priority.IdUsuario = @user_change
              # @priority.save
              #

              sql_destroy = "delete from talertaxusr where IdGrupo = #{@idGrupo} and IdPerfil = #{@idPerfil} and Prioridad = #{@idPriority};"
              sql_insert = "insert into talertaxusr (IdGrupo, IdPerfil, IdAlerta, Prioridad, IdUsuario) values (#{@idGrupo}, #{@idPerfil}, 1, #{@idPriority}, '#{@user_change}');"

              @destroy = ActiveRecord::Base::connection.exec_query(sql_destroy)
              @insert = ActiveRecord::Base::connection.exec_query(sql_insert)
            end

            #Formulario para agregar una prioridad
            if params[:form_priority]
              @level_priority = params[:priority]
              @user_add = params[:users_add_priority]
              @filter = params[:idf]

              @priority = Txfilterxusr.new
              @priority.Txfiltro_id = @filter
              @priority.Prioridad = @level_priority
              @priority.IdUsuario = @user_add
              @priority.save
            end

            #Eliminación de prioridades
            if params[:delete]
              @idPriority = params[:priorityId]
              @filter = params[:idf]

              @priority = Txfilterxusr.find_by(:Id => @idPriority)
              @priority.destroy

              @priorities = Txfilterxusr.where(:Txfiltro_id => @filter).where.not(:Id => @idPriority).order(:Prioridad)
              @count = 0
              @priorities.each do |p|
                @count += 1
                p.Prioridad = @count
                p.save
              end
            end

            # Variables para la pagina
            @idGrupo = params[:idg]
            @idPerfil = params[:idp]

            @grupo = Tgrupos.find_by(:IdGrupo => @idGrupo)
            @perfil = Tperfiles.find_by(:IdPerfil => @idPerfil)

            @prioridades = Talertaxusr.where(:IdPerfil => @idPerfil, :IdGrupo => @idGrupo).order(:Prioridad)
            @cant_priority = @prioridades.count()
            @sig_priority = @cant_priority + 1
            @users = User.all
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end
end
