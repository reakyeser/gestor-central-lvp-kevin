console.log('Soy alertn');

$('.linkTran').on('click', function(){
   // alert('Moviiii link')

    let obseArea = $('#obseArea').val(),
        estado = $('#state_ob').val(),
        id = $(this).html();
    // alert(id)

    //Traducciones
    let titleAlert = $('#titleAlert').val(),
        datosAlert = $('#datosAlert').val(),
        loseData = $('#loseData').val(),
        btn_continue = $('#btn_continue').val(),
        btn_yes = $('#btn_yes').val(),
        btn_cancel = $('#btn_cancel').val();



    if( obseArea != "" || estado != "" ){
        swal({
                title: `${titleAlert}`,
                text: `${datosAlert}`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                // confirmButtonText: `${btn_yes}`,
                confirmButtonText: `${btn_continue}`,
                // cancelButtonText: `${btn_cancel}`,
                cancelButtonText: `${btn_cancel}`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `${titleAlert}`,
                            // text: `${yourSure}`,
                            text: `${loseData}`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            // confirmButtonText: `${btn_confir}`,
                            confirmButtonText: `${btn_continue}`,
                            // cancelButtonText: `${btn_cancel2}`,
                            cancelButtonText: `${btn_cancel}`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                // Se envia el ajax para eliminar el filtro
                                // window.location.href = `/tx_filters/${id}/edit`
                                window.location.href = `/alertn/alertTran?id=${id}`

                            }
                            else{
                                swal(`Cancelado`,`Tus datos estan seguros`, "error");
                            }
                        });

                } else {
                    swal(`Cancelado`,`Tus datos estan seguros`, "error");
                }
            });
    } else {
        window.location.href = `/alertn/alertTran?id=${id}`
    }

});
