/**
 * Created by aflores8 on 3/01/17.
 */


$(document).ready(function () {
    cierreSession();
});


function parar() {
    clearTimeout(myvar);
}


function cierreSession() {
    console.log("Soy cierre_session.js");
     myvar = setTimeout(function () {
        var expired = false;
        var contador = 60 * 1000
        var id = document.getElementById('currentUserId').value;

        setTimeout(function () {
            expired = true;
        }, contador);

        swal({
                title: "¿Are You There?",
                text: "Your session is about to expire.",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, still here!",
                cancelButtonText: "",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    if (expired) {
                        swal("Expired Session.", "Your session expired due to inactivity.", "warning");
                        $.ajax({
                                type: "GET",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                },
                                url: "/users/expire?id=" + document.getElementById('currentUserId').value,
                                success: function (result) {
                                    location.reload(true);
                                },
                                error: function (result) {
                                    location.reload(true);
                                }
                            }
                        );
                    } else {
                        swal("Ok!", "Your session is active.", "success");
                        cierreSession();
                    }
                }
            });
    }, 1000 * 240)
    }
;
