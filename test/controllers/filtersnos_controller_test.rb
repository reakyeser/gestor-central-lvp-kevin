require 'test_helper'

class FiltersnosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @filtersno = filtersnos(:one)
  end

  test "should get index" do
    get filtersnos_url
    assert_response :success
  end

  test "should get new" do
    get new_filtersno_url
    assert_response :success
  end

  test "should create filtersno" do
    assert_difference('Filtersno.count') do
      post filtersnos_url, params: { filtersno: { Fecha: @filtersno.Fecha, IdFiltro: @filtersno.IdFiltro, IdUsuario: @filtersno.IdUsuario } }
    end

    assert_redirected_to filtersno_url(Filtersno.last)
  end

  test "should show filtersno" do
    get filtersno_url(@filtersno)
    assert_response :success
  end

  test "should get edit" do
    get edit_filtersno_url(@filtersno)
    assert_response :success
  end

  test "should update filtersno" do
    patch filtersno_url(@filtersno), params: { filtersno: { Fecha: @filtersno.Fecha, IdFiltro: @filtersno.IdFiltro, IdUsuario: @filtersno.IdUsuario } }
    assert_redirected_to filtersno_url(@filtersno)
  end

  test "should destroy filtersno" do
    assert_difference('Filtersno.count', -1) do
      delete filtersno_url(@filtersno)
    end

    assert_redirected_to filtersnos_url
  end
end
