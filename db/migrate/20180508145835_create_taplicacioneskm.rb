class CreateTaplicacioneskm < ActiveRecord::Migration[5.0]
  def change
    create_table :taplicacioneskm do |t|
      t.integer :IdAplicacion
      t.string :Nombre
      t.string :Descripcion
      t.string :Version
      t.integer :idProducto
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
