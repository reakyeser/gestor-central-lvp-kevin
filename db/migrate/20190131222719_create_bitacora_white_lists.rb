class CreateBitacoraWhiteLists < ActiveRecord::Migration[5.0]
  def change
    create_table :bitacora_white_lists do |t|
      t.integer :IdUsuario
      t.boolean :Estado
      t.integer :IdLista
      t.string :Hora
      t.string :Fecha

      t.timestamps
    end
  end
end
