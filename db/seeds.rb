# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ##################### VISTAS BASE ######################
vista = View.new
vista.name = "Home"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "Alerting and productivity levels"
vista.crear = 1
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "New Alert Transaccional"
vista.crear = 1
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "New Alert Perfiles"
vista.crear = 1
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "Consult Alert Transaccional"
vista.crear = 1
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "Consult Alert Perfiles"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "Alert Release"
vista.crear = 0
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "General Search"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "Users"
vista.crear = 0
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "User Attention Priority Transaccional"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "User Attention Priority Perfiles"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "Profiles"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "Areas"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "Views"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save



##################### AREA CENTRAL ######################
# area = Area.new
# area.name = "Central"
# area.save

##################### PERFIL ADMINISTRADOR CENTRAL ######################
# perfil = Profile.new
# perfil.name = 'Administrator Central'
# perfil.area_id = 1
# perfil.flag = 0
# perfil.save
#
permiso = Permission.new
permiso.view_id = 1
permiso.view_name = View.find(1).name # Home
permiso.crear = nil
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 2
permiso.view_name = View.find(2).name #Alert Levels
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 3
permiso.view_name = View.find(3).name #New Alert transaccional
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 4
permiso.view_name = View.find(4).name #New Alert Perfiles
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 5
permiso.view_name = View.find(5).name #Consult Alert transaccional
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 6
permiso.view_name = View.find(6).name #Consult Alert perfiles
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 7
permiso.view_name = View.find(7).name # Alert Release
permiso.crear = nil
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 8
permiso.view_name = View.find(8).name #General Search
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 9
permiso.view_name = View.find(9).name #Users
permiso.crear = nil
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 10
permiso.view_name = View.find(10).name #User Attention Priority Tran
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 11
permiso.view_name = View.find(11).name #User Attention Priority Perf
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 12
permiso.view_name = View.find(12).name #Profiles
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 13
permiso.view_name = View.find(13).name #Areas
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 14
permiso.view_name = View.find(14).name #Views
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

##################### PERFIL Default CENTRAL ######################
# perfil = Profile.new
# perfil.name = 'Default Central'
# perfil.area_id = 1
# perfil.flag = 1
# perfil.save

permiso = Permission.new
permiso.view_id = 1
permiso.view_name = View.find(1).name # Home
permiso.crear = nil
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 2
permiso.view_name = View.find(2).name #Alert Levels
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 3
permiso.view_name = View.find(3).name #New Alert transaccional
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 4
permiso.view_name = View.find(4).name #New Alert Perfiles
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 5
permiso.view_name = View.find(5).name #Consult Alert transaccional
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 6
permiso.view_name = View.find(6).name #Consult Alert perfiles
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 7
permiso.view_name = View.find(7).name #AlertRelease
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 8
permiso.view_name = View.find(8).name #General Search
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 9
permiso.view_name = View.find(9).name #Users
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 10
permiso.view_name = View.find(10).name #User Attention Priority Tran
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 11
permiso.view_name = View.find(11).name #User Attention Priority Perf
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 12
permiso.view_name = View.find(12).name #Profiles
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 13
permiso.view_name = View.find(13).name #Areas
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 14
permiso.view_name = View.find(14).name #Views
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save